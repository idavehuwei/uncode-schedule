function putsid(sid, app){
	$("#sid").val(sid);
	$("#sapp").val(app);
}

function changeStatus(){
	var id = $("#sid").val();
	var app = $("#sapp").val();
	var stat = $("#stat").val();
	console.log('/admin/status/' + app);
	console.log('id:' + id);
	console.log('status:' + stat);
	var url = "/admin/app/"+app+"/"+id+"/"+stat;
	$.ajax({
		url: url,
		type: 'get',
		dataType: 'json',
		success: function(data){
			console.log('overed');
			$("#myModal").modal('hide')
			queryList();
		}
	});
	
}

function queryList(){
	$.ajax({
		url: '/admin/home',
		type: 'get',
		dataType: 'json',
		success: function(data){
			$('#appCount').text(data['appCount'] || 'empty');
			$('#nodeCount').text(data['nodeCount'] || 'empty');
			$('#enableNodeCount').text(data['enableNodeCount'] || 'empty');
			//console.log('ok');
		}
	});
	
	$.ajax({
		url: '/admin/apps',
		type: 'get',
		dataType: 'json',
		success: function(data){
			console.log(data);
			for(var i=0; i<data.list.length; i++){
				$("#dataList  tr:not(:first)").html("");
				//for(var j=0; j<data.list[i].instance.length; j++){
					var trStr = "<tr>"+
								"<td>"+ data.list[i].name +"</td>\n"+
								"<td><span class=\"label ";
					if(data.list[i].status == 'UP'){
						trStr +="label-success\">";
					} else { 
						trStr +="label-danger\">";
					}
						trStr+= data.list[i].status+"</span></td>\n";
						trStr+="<td>"+data.list[i].ipAddr+"</td>\n";
						trStr+="<td><a href='" + data.list[i].info + "' target=\"_blank\">" +data.list[i].instanceId+"</a></td>\n";
						trStr+="<td>"+data.list[i].registrationTimestamp+"</td>\n";
						trStr+="<td>"+data.list[i].serviceUpTimestamp+"</td>\n";
						trStr+="<td>"+data.list[i].lastUpdatedTimestamp+"</td>\n";
						trStr+="<td>"+data.list[i].renewalIntervalInSecs+"</td>\n";
						trStr+="<td>"+data.list[i].version+"</td>\n";
						trStr+="<td>";

						trStr +="<a data-toggle=\"modal\" href=\"#myModal\" onclick=\"putsid('"+ data.list[i].instanceId + "','" + data.list[i].name +"')\">"+
								"	<span class=\"label label-danger\">Update</span>"+
								"</a>";
						trStr+="</td>\n"+
								"</tr>\n";
					$('#dataList').append(trStr);
					console.log(trStr);	
				//}
			}

		}
	});
	
	
}

(function(){
	$(function(){
		queryList();
	});
})();